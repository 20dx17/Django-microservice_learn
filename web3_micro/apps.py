from django.apps import AppConfig


class Web3MicroConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'web3_micro'
