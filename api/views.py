from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse,HttpResponseRedirect
from django.http.response import json
from django.template import loader,response
from .models import Member

def api(request):
	mem=Member.objects.all().values()
	temp=loader.get_template('index.html')
	return JsonResponse({'data':list(mem)})


def user(request):
	temp=loader.get_template('form.html')
	return render(request,'form.html')


def add_user(request):
	a=request.POST['fname']
	b=request.POST['lname']
	k=Member(firstName=a,LastName=b)
	k.save()
	return redirect('/')
