from django.urls import path
from . import views
urlpatterns=[
	path('api/',views.api,name='api'),
	path('register',views.add_user,name='register'),
	path('user/',views.user,name='user'),
]