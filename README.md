# Django Microservice Repo for Learning

### This Repo is created and maintained by Rohith aka Rohi

##### Download this project using the git clone method using 

`git clone https://gitlab.com/20dx17/Django-microservice_learn.git`

##### Change the branch

`git checkout rohi_dev`

##### Run the project

1. Windows
`py manage.py runserver`

2. Linux/OSX
`python3 manage.py runserver` or `./manage.py runserver`